﻿using UnityEngine;
using System.Collections;

public class GameOverScript : MonoBehaviour {
	Rect scoreRect;
	float _scrnWidth;
	float _scrnHeight;
	public AudioClip song;

	// Use this for initialization
	void Start () {
		audio.clip = song;
		audio.Play ();
	}
	
	// Update is called once per frame
	void Update () {
		_scrnWidth = Screen.width;
		_scrnHeight = Screen.height;
		scoreRect = new Rect (Pcnt(55, _scrnWidth), Pcnt(60, _scrnHeight), Pcnt(15, _scrnWidth), Pcnt(4, _scrnHeight));
		if (Input.GetKey(KeyCode.Escape)) {
			Application.Quit();
		}
	}

	void OnGUI() {
		GUI.Label (scoreRect, PlayerPrefs.GetInt("score").ToString());
		//GUI.Button (scoreRect, "HERE");
	}

	float Pcnt(float pcnt, float total) {
		return pcnt * (total / 100);
	}
}

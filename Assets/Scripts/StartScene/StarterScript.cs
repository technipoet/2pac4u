﻿using UnityEngine;
using System.Collections;

public class StarterScript : MonoBehaviour {

	Rect brettRect;
	Rect billyRect;
	float _scrnWidth;
	float _scrnHeight;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		brettRect = new Rect (Pcnt(25, _scrnWidth), Pcnt(38, _scrnHeight), Pcnt(17, _scrnWidth), Pcnt(40, _scrnHeight));
		billyRect = new Rect (Pcnt(57, _scrnWidth), Pcnt(38, _scrnHeight), Pcnt(17, _scrnWidth), Pcnt(40, _scrnHeight));

	}

	void OnGUI() {
		_scrnWidth = Screen.width;
		_scrnHeight = Screen.height;
		if (GUI.Button (brettRect, "", GUIStyle.none)) {
			PlayerPrefs.SetInt("billy", 0);
			Application.LoadLevel ("TestScene");
		}
		if (GUI.Button (billyRect, "", GUIStyle.none)) {
			PlayerPrefs.SetInt("billy", 1);
			Application.LoadLevel("TestScene");
		}
	}

	float Pcnt(float pcnt, float total) {
		return pcnt * (total / 100);
	}
}

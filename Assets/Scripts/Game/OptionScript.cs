﻿using UnityEngine;
using System.Collections;

public class OptionScript : MonoBehaviour {
	Sprite[] WordArray;
	public Sprite word0;
	public Sprite word1;
	public Sprite word2;
	public Sprite rhymeWord;
	public Sprite sentence;
	public int rightAns;

	// Use this for initialization
	void Start () {
		WordArray = new Sprite[3];
		WordArray [0] = word0;
		WordArray [1] = word1;
		WordArray [2] = word2;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public Sprite[] GetArray() {
		return WordArray;
	}

	public int GetAnswer() {
		return rightAns;
	}

	public Sprite GetSentence() {
		return sentence;
	}
	public Sprite GetRhyme() {
		return rhymeWord;
	}
}

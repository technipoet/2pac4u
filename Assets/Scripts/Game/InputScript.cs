﻿using UnityEngine;
using System.Collections;

public class InputScript : MonoBehaviour {

	/*
	 * This script is for easy management of all inputs
	 */ 
	void update () {
		if (Input.GetKey(KeyCode.Escape)) {
			Application.LoadLevel ("gameover");
		}
	}
	public bool P1Left() {
		return Input.GetKey (KeyCode.LeftArrow);
	}
	public bool P1Right() {
		return Input.GetKey (KeyCode.RightArrow);
	}
	public bool P1Up() {
		return Input.GetKeyDown (KeyCode.UpArrow);
	}
	public bool P1Down() {
		return Input.GetKeyDown (KeyCode.DownArrow);
	}

}

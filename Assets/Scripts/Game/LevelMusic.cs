﻿using UnityEngine;
using System.Collections;

public class LevelMusic : MonoBehaviour {
	public AudioClip song;
	public float beatInterval;
	public int loops;
	int loopCounter = 0;
	float halfInterval;
	float _scrnWith;
	float _scrnHeight;
	public Texture border;
	Rect borderRect;
	float errorTime;
	int multiplier;
	public InputScript _ctrl;

	Rect pacRect;
	int chosenMult;
	float pacTimer = 1.5f;
	float pacLastTime = 0;
	string pacSaying;
	bool rightAns = false;
	bool started = false;

	// Use this for initialization
	void Start () {
		audio.clip = song;
		SetScreen ();
	}
	
	// Update is called once per frame
	void Update () {

		halfInterval = beatInterval / 2;
		errorTime = Time.time % beatInterval < halfInterval ?
			Time.time % beatInterval : halfInterval - (Time.time % halfInterval);
		//Debug.Log (errorTime);
		if (errorTime < .01) {
			//this is for finding the exact beat
				Debug.Log ("Perfect");
			if (!started) {
				audio.Play();
				started = true;
			}
			if (started && loopCounter < loops && !audio.isPlaying) {
				audio.clip = song;
				audio.Play();
				loopCounter++;
			}
		}
		if (errorTime < beatInterval / 64) {
			//2Spooky
			multiplier = 3;
		} else if (errorTime < beatInterval / 32) {
			//spooky
			multiplier = 2;
		} else if (errorTime < beatInterval / 16) {
			//boo
			multiplier = 1;
		} else {
			//GAH
			multiplier = 0;
		}

		if (!audio.isPlaying && started) {
			//Application.Quit();
			//Load end screen with Application.LoadLevel (); with a string as param
			Application.LoadLevel ("gameover");
		}
	}

	void SetScreen() {
		_scrnWith = Screen.width;
		_scrnHeight = Screen.height;
		borderRect = new Rect (0, 0, _scrnWith, _scrnHeight);
		pacRect = new Rect (Pcnt (80, _scrnWith), Pcnt (20, _scrnHeight), Pcnt (10, _scrnWith), Pcnt (10, _scrnHeight));

	}

	void OnGUI() {
		/*
		if (Time.time % beatInterval < .021) {
			GUI.DrawTexture (borderRect, border);
		}*/
		PacsOpinion ();
		if (errorTime < (beatInterval / 64)) {
			GUI.DrawTexture (borderRect, border);
		}

	}

	void PacsOpinion() {
		pacLastTime = _ctrl.P1Down() ? Time.time : pacLastTime;
		if (_ctrl.P1Down()) {
			chosenMult = multiplier;
		}
		if (Time.time < pacLastTime+pacTimer && rightAns) {
			switch (chosenMult) {
				case 0:
					pacSaying = "GAHH!";
					break;
				case 1:
					pacSaying = "Boo!";
					break;

				case 2:
					pacSaying = "Spooky!";
					break;
				case 3:
					pacSaying = "2 SPOOKY!!";
					break;
				default:
					pacSaying = "Default!";
				break;
			}
			GUI.Label(pacRect, pacSaying);
		}
		else {
			rightAns = false;
		}
	}


	public int GetMultiplier(bool rightAnswer) {
			rightAns = rightAnswer;
			return multiplier;

	}
	float Pcnt(float pcnt, float total) {
		return pcnt * (total / 100);
	}
}

﻿using UnityEngine;
using System.Collections;

public class WordScript : MonoBehaviour {
	public string wordString;
	public int wordNum;
	public TotalWordScript master;

	public SpriteRenderer render;

	// Use this for initialization
	void Start () {
		//render = GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetSprite(Sprite s) {
		render.sprite = s;
	}

	void OnTriggerEnter2D(Collider2D col) {
		master.WordHit (wordNum);
	}

}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TotalWordScript : MonoBehaviour {
	public WordScript word1;
	public WordScript word2;
	public WordScript word3;
	public RhymeWord rhymeWord;
	public RhymeSentScript sentence;
	public List<OptionScript> optionList;
	public LevelMusic music;
	int rightAns;
	int option = 0;
	float _scrnWith;
	float _scrnHeight;
	Sprite[] words;
	Sprite rhymeSprite;
	//Sprite rhymeSent;
	OptionScript curOpt;
	public ScoreGlow billyGlow;
	public ScoreGlow brettGlow;
	ScoreGlow _glow;
	Rect scoreRect;
	Rect comboRect;


	int score = 0;
	int combo = 1;

	// Use this for initialization
	void Start () {
		SetValues ();
		_glow = PlayerPrefs.GetInt ("billy") == 1 ? billyGlow : brettGlow;

	}
	
	// Update is called once per frame
	void Update () {
		SetScreen ();
		PlayerPrefs.SetInt ("score", score);
	}

	public void WordHit(int word) {
		int mulitplier = music.GetMultiplier (word == rightAns);
		if (word == rightAns && mulitplier != 0) {
			combo++;
			score += ((combo * combo) * mulitplier);
		}
		else {
			combo = 1;
		}
		_glow.SetPower (combo);
		if (option + 1 == optionList.Count) {
			shuffleList();
		}
		option = option + 1 == optionList.Count ? 0 : option+1;
		Debug.Log ("Score:"+score);
		Debug.Log ("Combo:"+combo);

		SetValues ();
	}

	void SetValues() {
		curOpt = optionList [option];
		words = curOpt.GetArray();
		rhymeSprite = curOpt.GetRhyme ();
		//rhymeSent = curOpt.GetSentence ();
		//sentence.SetSprite (rhymeSent);
		rhymeWord.SetSprite(rhymeSprite);


		word1.SetSprite (words[0]);
		word2.SetSprite (words[1]);
		word3.SetSprite (words[2]);
		rightAns = curOpt.GetAnswer ();
	}

	void OnGUI() {
		GUI.Label (scoreRect, "Score: "+ score);
		int comboShow = combo == 1 ? 0 : combo;
		GUI.Label (comboRect, "Combo: "+ comboShow);

	}

	void SetScreen() {
		_scrnWith = Screen.width;
		_scrnHeight = Screen.height;
		scoreRect = new Rect (Pcnt (90, _scrnWith), Pcnt(5, _scrnHeight), Pcnt(10, _scrnWith), Pcnt (10, _scrnHeight));
		comboRect = new Rect (Pcnt (90, _scrnWith), Pcnt (8, _scrnHeight), Pcnt (10, _scrnWith), Pcnt (10, _scrnHeight));

	}

	float Pcnt(float pcnt, float total) {
		return pcnt * (total / 100);
	}


	void shuffleList() {
		OptionScript temp;
		for (int i = 0; i < optionList.Count; i++) {
			int val = (int)(Random.value % optionList.Count);
			temp = optionList[val];
			optionList[val] = optionList[0];
			optionList[0] = temp;
		}
	}


}

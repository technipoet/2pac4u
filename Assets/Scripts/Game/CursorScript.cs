﻿using UnityEngine;
using System.Collections;

public class CursorScript : MonoBehaviour {
	public InputScript _ctr;
	float velCutVal = 1; //value to help player change directions quickly
	float forceMultiplier = 1;
	public TotalWordScript wordTracker;
	float yHome;
	public bool billy;
	public GameObject Billy;
	public GameObject Brett;
	Animator anim;
	bool spit = false;
	// Use this for initialization
	void Start () {
		yHome = rigidbody2D.position.y;
		if (PlayerPrefs.GetInt("billy") == 1) {
			Billy.SetActive(true);
			anim = Billy.GetComponent<Animator>();
		} 
		else {
			Brett.SetActive(true);
			anim = Brett.GetComponent<Animator>();
		}
	}

	void FixedUpdate () {
		rigidbody2D.position = new Vector2 (rigidbody2D.position.x, yHome);
		float yTemp = rigidbody2D.velocity.y;
		if (rigidbody2D.velocity.x > 30) {
			rigidbody2D.velocity = new Vector2(30, yTemp);
		}
		if (rigidbody2D.velocity.x < -30) {
			rigidbody2D.velocity = new Vector2(-30, yTemp);
		}

	}
	// Update is called once per frame
	void Update () {
		if (_ctr.P1Left()) {
			forceMultiplier = rigidbody2D.velocity.x > velCutVal ? 4 : 1;
			rigidbody2D.AddForce(new Vector2(-12, 0) * forceMultiplier);
		}
		if (_ctr.P1Right()) {
			forceMultiplier = rigidbody2D.velocity.x < -velCutVal ? 4 : 1;
			rigidbody2D.AddForce(new Vector2(12, 0) * forceMultiplier);
		}
		anim.SetBool("spit", _ctr.P1Down() && !spit);
		spit = _ctr.P1Down() && !spit;

	}

}

﻿using UnityEngine;
using System.Collections;

public class RhymeWord : MonoBehaviour {
	public InputScript _ctr;
	bool falling = false;
	Vector2 downVel = new Vector2(0, -30);
	float yHome;
	public Rigidbody2D parent;
	public SpriteRenderer render;
	float lastDown;
	float coolDown = .4f;
	// Use this for initialization
	void Start () {
		yHome = transform.position.y;
		render = GetComponent<SpriteRenderer> ();
		lastDown = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (!falling) {
			rigidbody2D.position = new Vector2(parent.position.x, yHome);
			if (_ctr.P1Down() && Time.time >= lastDown + coolDown) {
				lastDown = Time.time;
				falling = true;
				rigidbody2D.velocity = downVel;
			}
		}
	}


	void OnTriggerEnter2D(Collider2D col) {
		if (col.collider2D.tag == "Word") {
			rigidbody2D.position = new Vector2(parent.position.x, yHome);
			rigidbody2D.velocity = new Vector2(0, 0);
			falling = false;

		}
	}

	public void SetSprite(Sprite s) {
		render.sprite = s;
	}
}

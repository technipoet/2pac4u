﻿using UnityEngine;
using System.Collections;

public class ScoreGlow : MonoBehaviour
{

    public float ScorePower = 0;
    public ParticleSystem glow;
    public ParticleSystem lefteye;
    public ParticleSystem righteye;
    public ParticleSystem leftsparkle;
    public ParticleSystem rightsparkle;

	void Update ()
	{
	    glow.startSize = ScorePower;
        lefteye.startSize = ScorePower;
        righteye.startSize = ScorePower;
        leftsparkle.startSize = ScorePower;
        rightsparkle.startSize = ScorePower;
	}

	public void SetPower(int power) {
		ScorePower = (power - 1)/3;
	}
}
